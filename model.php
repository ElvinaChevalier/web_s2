<?php
// connexion a la BDD
function connection() {
	try {
		$cnx = new PDO('mysql:host=localhost;dbname=survimac;charset=utf8','root','');
		$cnx ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch (PDOException $e) {
		echo 'Échec lors de la connexion : ' . $e->getMessage();
	}
	
	return $cnx;
}

// Fonction Remplissage BDD

function add_a_joueur($id, $nom, $prenom, $generation, $team) {
	$cnx = connection();
	$rqt1 = $cnx->prepare('update `joueur` SET `nom`=?,`prenom`=?,`H`=10,`M`=10,`F`=10,`P`=10,`generation`=?,`etat`=1 WHERE `id`=?');
	$rqt1->execute(array($nom, $prenom, $generation, $id));
	
	return 1;
}

function add_joueur_in_team($id_joueur, $id_team){
	$cnx = connection();
	$rqt = $cnx->prepare("INSERT INTO `corres_joueurs_equipes`(`id_joueur`, `id_equipe`) VALUES (?,?)");
	$rqt->execute(array($id_joueur, $id_team));
}

// Fonction Modif joueur

function update_a_joueur($id, $H, $M, $F, $P, $etat){
	$cnx = connection();
	$rqt1 = $cnx->prepare('update `joueur` SET `H`=?,`M`=?,`F`=?,`P`=?,`etat`=? WHERE `id`=?');
	$rqt1->execute(array($H, $M, $F, $P, $etat, $id));
}

// Fonctions GET 

function get_joueur_team($id_team){ 
	$cnx = connection();
	$rqt = $cnx->prepare("SELECT * FROM joueur WHERE id=?");
	$rqt->execute(array($id_team));
	return $rqt->fetch(PDO::FETCH_ASSOC);
}

function get_corresp_joueur_team($id_team){
	$cnx = connection();
	$rqt = $cnx->prepare("SELECT `id_joueur` as joueur FROM `corres_joueurs_equipes` WHERE `id_equipe`=?");
	$rqt->execute(array($id_team));
	return $rqt->fetchall(PDO::FETCH_ASSOC);
}

function get_a_joueur($id_joueur){
	$cnx = connection();
	$rqt = $cnx->prepare("SELECT * FROM joueur WHERE id=?");
	$rqt->execute(array($id_joueur));
	return $rqt->fetch(PDO::FETCH_ASSOC);
}

function get_a_event_joueur($id_event_joueur){ 
	$cnx = connection();
	$rqt = $cnx->prepare("SELECT * FROM ev_joueurs WHERE id=?");
	$rqt->execute(array($id_event_joueur));
	return $rqt->fetch(PDO::FETCH_ASSOC);
}

function get_a_event_globaux($id_event_globaux){
	$cnx = connection();
	$rqt = $cnx->prepare("SELECT * FROM ev_globaux WHERE id=?");
	$rqt->execute(array($id_event_globaux));
	return $rqt->fetch(PDO::FETCH_ASSOC);
}

// Fonctions count 

function count_event_joueurs(){
	$cnx = connection();
	$rqt = $cnx->prepare("SELECT COUNT(*) FROM ev_joueurs");
	$rqt->execute();
	return $rqt->fetch(PDO::FETCH_ASSOC);
}

function count_event_globaux(){
	$cnx = connection();
	$rqt = $cnx->prepare("SELECT COUNT(*) FROM ev_globaux");
	$rqt->execute();
	return $rqt->fetch(PDO::FETCH_ASSOC);
}

// Fonctions de nettoyage de la table de correspondance

function truncate_teams(){
	$cnx = connection();
	$rqt = $cnx->prepare("truncate `corres_joueurs_equipes`");
	$rqt->execute();
}