## Surv'IMAC = IMAC Survival (titre provisoire)
_c'est le plus beau des projets_

### Groupe et Fonctionnement 
Membres du groupe / rôle : 
- Elvina Chevalier
- Sacha Chouvin 
- Jolan Goulin 
- Margot Fauchon
- Quentin Mesquita

### Description du jeu 
**Pitch :** Le projet est un jeu type "Hunger Game",
L'utilisateur commence par choisir les personnages qui vont participer à la partie et les teams, puis lance le jeu.
Des évênements aléatoires surviennent chaque jour et affectent l'environnement (le campus de la fac), les teams ou les personnages directement.
Au fil des jours, certains personnages vont être éliminés du jeu (renvoi pour cause de mauvaises peformances scolaires, dépression causée par une jauge de 'fun' ou de 'santé mentale' trop basse, mort naturelle quand les besoins vitaux ne sont pas satisfaits), qui sera le dernier survivant ?  

**Interface utilisateur : **
- un bouton pour lancer la journée suivante une fois qu'il a lu les évènements de la journée
- des boutons 'pouvoir' à usage unique pour pouvoir influencer le jeu (avec un cooldown) 

**La Data Base : **
- liste des tables : joueur, teams, évènements globaux, évènements joueurs 


### Liens 
hébergement par la fac ici : https://perso-etudiant.u-pem.fr/~schouvin

brainstorm ici : https://docs.google.com/document/d/118uFq1r6v1EwfBRbmGgdJ3FMsVQGmYwxCmFAFi4HSAg/edit?usp=sharing

suivi sérieux ici : https://docs.google.com/document/d/1cjXYRGCEbxnvICb2s7oi2SVmJqwms0lENsCJO4MstU0/edit?usp=sharing
