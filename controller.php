<?php
require_once ('model.php');

// Remplissage de la BDD 

function addAJoueur($form){
    $joueur = json_decode($form, true);
    return json_encode(add_a_joueur($joueur['id'],  $joueur['nom'],  $joueur['prenom'], $joueur['generation'], $joueur['team'] ));
}

// Fonctions GET 

function getAJoueur($id_joueur){
    return json_encode(get_a_joueur($id_joueur));
}

function getATeam($id_team){
    return json_encode(get_joueurs_team($id_team));
}

function addJoueurInTeam($id_joueur, $id_team){
    return json_encode(add_joueur_in_team($id_joueur, $id_team));
}

function getCorrespJoueurTeam($id_team){
    return json_encode(get_corresp_joueur_team($id_team));
}


function getAnEventJoueur($id_event){
    return (json_encode(get_a_event_joueur($id_event)));
}

function getAnEventGlobal($id_event){
    return (json_encode(get_a_event_globaux($id_event)));
}

// Fonctions Modif 

function updateAJoueur($form){
    $infos = json_decode($form, true);
    return json_encode(update_a_joueur($infos['id'], $infos['H'], $infos['M'], $infos['F'], $infos['P'], $infos['etat'] ));
}

// Fonctions count 

function countEventGlobaux(){
    return (json_encode(count_event_globaux()));
}

function countEventJoueurs(){
    return (json_encode(count_event_joueurs()));
}

// Fonction de nettoyage 

function truncateTeams(){
    return json_encode(truncate_teams());
}

