/* rappel : #id et .class */

/* Récupération des éléments du HTML */

const section_menu = document.querySelector('#menu');
const section_parametres = document.querySelector('#param_section');
const section_jeu = document.querySelector('#game_section');
const section_fin = document.querySelector('#end_section');

const param_teams = document.querySelector('#param_teams');

const game_global_event = document.querySelector('#global_event');
const game_global_teams = document.querySelector('#game_global_teams');
const game_indiv_event = document.querySelector('#joueur_event');
const game_indiv_teams = document.querySelector('#game_indiv_teams');

const btn_start = document.querySelector('#start_game');
const btn_launch = document.querySelector('#param_launch_game');
const btn_param_alea = document.querySelector('#param_alea');
const btn_event = document.querySelector('#game_global_next');
const btn_day = document.querySelector('#game_indiv_next');
const btn_restart = document.querySelector('#restart_game');

/* Constantes */
const nb_teams = 6;
const nb_event_globaux = 40;
const nb_event_joueurs = 40;
const nb_joueurs = 12;

/* Variables */
var nb_joueur_en_vie = 12;
var current_day = 0;

/* Préparation du document */
Document.prototype.ready = callback => {
    if (callback && typeof callback == 'function') {
        document.addEventListener("DOMContentLoaded", () => {
            if (document.readyState === "interactive" || document.readyState === "complete" ) {
                return callback();
            }
        });
    }
};

/* Fonction élémentaire */
function displayOrHide (element){
    element.classList.toggle("hidden_element");
};

/* Fonctions qui convertissent les infos entieres de la BDD en char pour le front */

function getId (i,j){
    if (j=='a')
        return i*2-1;
    else if (j=='b')
        return 2*i;
}

function getGeneration(generation){
    switch (generation){
        case "0":
            return "Tout le monde";
        case "1":
            return "Team 1";
        case "2":
            return "Team 2";
        case "3":
            return "Team 3";
        case "4":
            return "Team 4";
        case 5:
            return "Team 5";
        case 6:
            return "Team 6";
        case "7":
            return "Année 1";
        case "8":
            return "Année 2";
        case "9":
            return "Année 3";
        case "10":
            return "Papimac";
        case "11":
            return "Prof";
        default:
            return "?";
    }
}

function getJaugeState(state){
    switch (state){
        case "0":
            return "empty";
        case "1":
            return "ten";
        case "2":
            return "twenty";
        case "3":
            return "thirty";
        case "4":
            return "forty";
        case "5":
            return "fifty";
        case "6":
            return "sixty";
        case "7":
            return "seventy";
        case "8":
            return "eighty";
        case "9":
            return "ninety";
        default:
            return "full";
    }
}

function getCategory(cat){
    switch (cat){
        case 'fun':
            return 'F';
        case 'perf':
            return 'P';
        case 'health':
            return 'H';
        case 'mental':
            return 'M';
    }
}

/* Communication avec le router */

function pushPerso (i,j){
    const form = {};
    form.id = getId(i,j)
    form.nom = document.querySelector("#param_perso"+i+j+"_nom").value;
    form.prenom = document.querySelector("#param_perso"+i+j+"_prenom").value;
    form.generation = document.querySelector("#param_perso"+i+j+"_generation").value;
    form.team = i;

    fetch("./router.php/joueur", { method: 'PUT', body: JSON.stringify(form)})
        .then (response => response.json())
        .then (data => {}) 
        .catch(error => { console.log(error) });
}

function updatePerso(infos){
    fetch("./router.php/joueur/"+infos.id, { method: 'POST', body: JSON.stringify(infos)})
        .then(response => response.json())
        .then(data => {
            var i, j;
            var index = parseInt(infos.id);
    
            if (index%2==0){
                i = index /2;
                j = 'b';
            }else {
                i = (index+1) /2;
                j = 'a';
            }

            majGamePerso('global', i,j);
            majGamePerso('indiv', i,j);
         })
        .catch(error => { console.log(error) });    
}

function associateJoueurTeam(id_joueur, id_team){
    fetch("./router.php/team/"+id_joueur+"/"+id_team, { method: 'PUT'})
        .then(response => response.json())
        .then (data => { })
        .catch(error => { console.log(error) });
}

function dissociateTeams(){
    fetch("./router.php/team/", { method: 'DELETE'})
        .then(response => response.json())
        .then (data => { })
        .catch(error => { console.log(error) });
}

function applyEffectOnPlayersGeneration(generation, H,M,F,P){
    fetch("./router.php/team/"+generation, { method : 'GET'})
        .then(response => response.json())
        .then(data => {
            console.log(data);
            data.forEach(element => {
                etat_joueur_globaux(element.joueur,H,M,F,P);
            });
        })
        .catch(error => { console.log(error) });        
}

function applyEffectOnPlayers($id_joueur, $H, $M, $F, $P){
    const $form = {};
    fetch("./router.php/joueur/"+$id_joueur, { method: 'GET'})
        .then(response => response.json())
        .then (data => { 
            $form.id = data.id;
            $form.H = parseInt(data.H) + parseInt($H);
            if ($form.H >= 10)
                $form.H = 10;
            $form.M = parseInt(data.M) + parseInt($M);
            if ($form.M >= 10)
                $form.M = 10;
            $form.F = parseInt(data.F) + parseInt($F);
            if ($form.F >= 10)
                $form.F = 10;
            $form.P = parseInt(data.P) + parseInt($P);
            if ($form.P >= 10)
                $form.P = 10;
            if($form.H<=0 || $form.M<=0 || $form.F<=0 || $form.P<=0 ){
                $form.etat=0;
                $form.H = 0;
                $form.F = 0;
                $form.M = 0;
                $form.P = 0;
                nb_joueur_en_vie -= 1;
            }
                
            else 
                $form.etat=1;

            updatePerso($form);
        })
        .catch(error => { console.log(error) });
}

function tire_event_global(){
    var id = Math.floor(Math.random()*(nb_event_globaux) - 0.01) + 1;

    fetch("./router.php/event-global/"+id, { method: 'GET'})
        .then(response => response.json())
        .then (data => { 
            console.log(getGeneration(data.generation_touchee));
            document.querySelector("#current_global_event_title").innerHTML = data.nom;
            document.querySelector("#current_global_event_description").innerHTML = data.description;
            document.querySelector("#current_global_event_cible").innerHTML = "Touche : "+ getGeneration(data.generation_touchee);
            document.querySelector("#current_global_event_effect").innerHTML = "Effets : "+data.effet_F+" de fun, "+data.effet_H+" de santé, "+data.effet_P+" de performances, "+data.effet_M+" de mental";
            applyEffectOnPlayersGeneration(data.generation_touchee, data.effet_H, data.effet_M, data.effet_F, data.effet_P);
        })
        .catch(error => { console.log(error) }); 
}

function tire_event_joueur(id_joueur){

    fetch("./router.php/joueur/"+id_joueur, { method: 'GET'})
        .then(response => response.json())
        .then (data => { 
            console.log("joueur "+data.id+" "+data.nom+" "+data.prenom);
            document.querySelector("#current_joueur_event_cible").innerHTML = "Joueur affecté : "+data.prenom+" "+data.nom;
        })
        .catch(error => { console.log(error) });
        
    var id_event = Math.floor(Math.random()*(nb_event_joueurs) - 0.01) + 1;

    fetch("./router.php/event-joueur/"+id_event, { method: 'GET'})
        .then(response => response.json())
        .then (data => { 
            document.querySelector("#current_joueur_event_title").innerHTML = data.nom;
            document.querySelector("#current_joueur_event_description").innerHTML = data.description;
            document.querySelector("#current_joueur_event_effect").innerHTML = "Effets : "+data.effet_F+" de fun, "+data.effet_H+" de santé, "+data.effet_P+" de performances, "+data.effet_M+" de mental";
            applyEffectOnPlayers(id_joueur, data.effet_H, data.effet_M, data.effet_F, data.effet_P);
        })
        .catch(error => { console.log(error) }); 
}

function etat_joueur(id){
    fetch("./router.php/joueur/"+id, { method: 'GET'})
        .then(response => response.json())
        .then (data => {
            if(data.etat==1){
                tire_event_joueur(id);
            }else{
                event_joueurs();
            }
        })
        .catch(error => { console.log(error) });
}

function etat_joueur_globaux(id,H,M,F,P){
    fetch("./router.php/joueur/"+id, { method: 'GET'})
        .then(response => response.json())
        .then (data => {
            if(data.etat==1)
                applyEffectOnPlayers(id,H,M,F,P);
            
        })
        .catch(error => { console.log(error) });
}
/* Fonction de lancement du jeu (partie choix des joueurs) */

function startClick(e){
    dissociateTeams();
    displayOrHide(section_menu);
    for (var i=1; i< nb_teams+1; i++){
        createParams(i);
    }
    displayOrHide(section_parametres);
    displayOrHide(document.querySelector('#accueil'));
}
btn_start.addEventListener('click', startClick);


/* Creation et remplissage de l'espace parametre (choix des 12 joueurs) */

function createParams(i){
    var newTeam = document.createElement('div');
    newTeam.id = "param_team"+i;
    newTeam.classList.add("param_team");

    var newTeamTitle = document.createElement('h3');
    newTeamTitle.classList.add("team_titre");
    newTeamTitle.innerHTML = "Team"+i;
    newTeam.appendChild(newTeamTitle);

    var newPersoA = createParamPerso(i, 'a');
    var newPersoB = createParamPerso(i, 'b');

    newTeam.appendChild(newPersoA);
    newTeam.appendChild(newPersoB);
    param_teams.appendChild(newTeam);
}

function createParamPerso(i, j){
    var newPerso = document.createElement('div');
    newPerso.id = "param_perso"+i+j;
    newPerso.classList.add("param_perso");
    var pp;
    if(j=='a')
        pp=2*i-1;
    else    
        pp=2*i;
    var newPP = document.createElement('img');
    newPP.setAttribute("src","src/"+pp+".png");
    newPerso.appendChild(newPP);

    newPerso.appendChild(createParamPersoName(i,j,"nom"));
    newPerso.appendChild(createParamPersoName(i,j,"prenom"));

    var select = document.createElement('select');
    select.id = "param_perso"+i+j+"_generation";
 
    select.options.add(createParamSelect("7", "Année1"));
    select.options.add(createParamSelect("8", "Année2"));
    select.options.add(createParamSelect("9", "Année3"));
    select.options.add(createParamSelect("10", "Papimac"));
    select.options.add(createParamSelect("11", "Prof"));

    newPerso.appendChild(select);

    return newPerso;
}

function createParamPersoName(i,j,name){
    var inputName = document.createElement('input');
    inputName.setAttribute("type", "text");
    inputName.id = "param_perso"+i+j+"_"+name;
    inputName.setAttribute("placeholder", name);
    return inputName;
}

function createParamSelect(value, text){
    var option = document.createElement("option");
    option.value = value;
    option.text = text;
    return option;
}

function fillParamPersoAlea (i,j){
    const id = getId(i,j);
    fetch("./router.php/joueur/"+id, { method: 'GET'})
        .then(response => response.json())
        .then (data => { 
            document.querySelector("#param_perso"+i+j+"_nom").placeholder = data.nom ;
            document.querySelector("#param_perso"+i+j+"_nom").value = data.nom ;
            document.querySelector("#param_perso"+i+j+"_prenom").placeholder = data.prenom ;
            document.querySelector("#param_perso"+i+j+"_prenom").value = data.prenom;
            document.querySelector("#param_perso"+i+j+"_generation").selectedIndex = Math.floor(Math.random()*5); 
        })
        .catch(error => { console.log(error) });    
}

function paramAleaClick(){
    console.log("la normalement on met des noms marrants à ta place !");
    for (var i=1; i<nb_teams+1; i++) {
        fillParamPersoAlea(i,'a');
        fillParamPersoAlea(i,'b');
    }
}
btn_param_alea.addEventListener('click', paramAleaClick);

/* Creation de l'espace jeu (en 2 parties : global et indiv) */

function createGameTeams(section, i){
    var newTeam = document.createElement('div');
    newTeam.id = section+"_game_team"+i;
    newTeam.classList.add("game_team");
    newTeam.innerHTML="Team"+i;

    newTeam.appendChild(createGamePerso(section, i, 'a'));
    newTeam.appendChild(createGamePerso(section, i, 'b'));

    if (section == "global")
        game_global_teams.appendChild(newTeam);
    else   
        game_indiv_teams.appendChild(newTeam);
}

function createGamePerso(section, i,j){
    var newPerso = document.createElement('div');
    newPerso.id = section+"_perso"+i+j;
    newPerso.classList.add("game_perso");

    var id = getId(i,j);

    var newPP = document.createElement('img');
    newPP.setAttribute("src","src/"+id+".png");
    newPerso.appendChild(newPP);

    var name = document.createElement('p');
    var generation = document.createElement('p');
    
    var jauges = document.createElement('div');
    jauges.classList.add("game_jauges")

//récupération et injection des données du joueur
    fetch("./router.php/joueur/"+id, { method: 'GET'})
        .then(response => response.json())
        .then (data => { 
            name.innerHTML = data.prenom+" "+data.nom;
            generation.innerHTML = getGeneration(data.generation);
            jauges.appendChild(createGameJauge(section, i,j,"fun",getJaugeState(data.F)));
            jauges.appendChild(createGameJauge(section, i,j,"health",getJaugeState(data.H)));
            jauges.appendChild(createGameJauge(section, i,j,"perf",getJaugeState(data.P)));
            jauges.appendChild(createGameJauge(section, i,j,"mental",getJaugeState(data.M))); 
            if (section == "global"){
                associateJoueurTeam(data.id, data.generation);
                associateJoueurTeam(data.id, 0);
                associateJoueurTeam(data.id, i);    
            }
        })
        .catch(error => { console.log(error) });

    newPerso.appendChild(name);
    newPerso.appendChild(generation);
    newPerso.appendChild(jauges);

    return newPerso;
}

function createGameJauge(section, i,j,cat,state) {
    var c = getCategory(cat);
    
    var newJauge = document.createElement('div');
    newJauge.id=section+"_jauge"+i+j+c;
    newJauge.value = 10;
    newJauge.classList.add("progress_bar");

    var newNiveau = document.createElement('div');
    newNiveau.id=section+"_niveau"+i+j+c;
    newNiveau.classList.add("niveau");
    newNiveau.classList.add(cat);
    newNiveau.classList.add(state);
    newJauge.appendChild(newNiveau);

    return newJauge;
}

/* Mise a jour de l'espace de jeu */

function majGamePerso(section, i, j){
    
    var id = getId(i,j);
    var perso = document.querySelector('#'+section+"_perso"+i+j);

    var jaugeFId = section+"_niveau"+i+j+'F';
    var jaugeHId = section+"_niveau"+i+j+'H';
    var jaugePId = section+"_niveau"+i+j+'P';
    var jaugeMId = section+"_niveau"+i+j+'M';

    var jaugeF = document.querySelector('#'+jaugeFId);
    var jaugeH = document.querySelector('#'+jaugeHId);
    var jaugeP = document.querySelector('#'+jaugePId);
    var jaugeM = document.querySelector('#'+jaugeMId);

    jaugeF.classList.remove('full', 'ninety', 'eighty', 'seventy', 'sixty', 'fifty', 'forty', 'thirty', 'twenty', 'ten', 'empty');
    jaugeH.classList.remove('full', 'ninety', 'eighty', 'seventy', 'sixty', 'fifty', 'forty', 'thirty', 'twenty', 'ten', 'empty');
    jaugeP.classList.remove('full', 'ninety', 'eighty', 'seventy', 'sixty', 'fifty', 'forty', 'thirty', 'twenty', 'ten', 'empty');
    jaugeM.classList.remove('full', 'ninety', 'eighty', 'seventy', 'sixty', 'fifty', 'forty', 'thirty', 'twenty', 'ten', 'empty');
   
    fetch("./router.php/joueur/"+id, { method: 'GET'})
        .then(response => response.json())
        .then (data => { 
            jaugeF.classList.add(getJaugeState(data.F));
            jaugeH.classList.add(getJaugeState(data.H));
            jaugeM.classList.add(getJaugeState(data.M));
            jaugeP.classList.add(getJaugeState(data.P));

            if (data.etat == "0")
                perso.classList.add("dead");
        })
        .catch(error => { console.log(error) }); 
}

/* Fonction de lancement du jeu */

function launchClick(e){
    displayOrHide(section_parametres);
    current_day += 1;

    for (var i=1; i<nb_teams+1; i++) {
        pushPerso(i,'a');
        pushPerso(i,'b');
    }

    for (var i=1; i<nb_teams+1; i++){
        createGameTeams("global", i);
        createGameTeams("indiv", i);
    }

    tire_event_global();

    displayOrHide(section_jeu);
}
btn_launch.addEventListener('click', launchClick);

/* Fonctions de gestion des évènements de la partie */

function event_joueurs(){   
    if (testFin())
        fin();
    else{
        var id_joueur = Math.floor(Math.random()*12+1 - 0.01);
        etat_joueur(id_joueur);
        displayOrHide(game_global_event);
        displayOrHide(game_indiv_event);
    }
}
btn_event.addEventListener('click', event_joueurs);

function event_global(){
    if (!testFin()){
        tire_event_global();

        var current_day_description = document.querySelector('#current_day');
        current_day += 1;
        current_day_description.innerHTML = "Jour " + current_day;
    
        displayOrHide(game_global_event);
        displayOrHide(game_indiv_event);
    
    } else {
        fin();
    }
}
btn_day.addEventListener('click', event_global);

function testFin(){
    if (nb_joueur_en_vie <= 1)
        return true;
    else 
        return false;
}

/* Fonctions de gestion de la fin de partie */

function fin(){
    displayOrHide(section_jeu);
    displayOrHide(section_fin);  
}

function restartClick(e){
    dissociateTeams();
    displayOrHide(section_menu);
    displayOrHide(document.querySelector('#accueil'));
    section_jeu.classList.add("hidden_element");
    displayOrHide(section_fin);
}
btn_restart.addEventListener('click', restartClick);
 
