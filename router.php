<?php

error_reporting( E_ALL );

require_once ('controller.php');
$page = explode('/',$_SERVER['REQUEST_URI']);
$method = $_SERVER['REQUEST_METHOD'];

//récupération des endpoints 

$endpoint = $page[3]; 
$complement = $page[4];

switch($endpoint) {
	case 'joueur' : 
		switch($method) {
			case 'GET' : 
				echo getAJoueur($complement);
				break;
			case 'POST' :
				$json = file_get_contents('php://input');
				echo updateAJoueur($json);
				break;
			case 'PUT' :
				$json = file_get_contents('php://input');
				echo addAJoueur($json);
				break;
			default:
				http_response_code('404');
				echo 'Diantre !';
		}
		break;
	
	case 'team' :
		switch ($method){
			case 'PUT' :
				echo addJoueurInTeam($complement, $page[5]);
				break;
			case 'DELETE' :
				echo truncateTeams();
				break;
			case 'GET' :
				echo getCorrespJoueurTeam($complement);
				break;
			default :
				http_response_code('404');
				echo 'Diantre !';
		}
		break;
	

	case 'event-global' : 
		switch($method) {
			case 'GET' : 
				echo getAnEventGlobal($complement);
				break;
			default:
				http_response_code('404');
				echo 'Diantre !';
		}
		break;

	case 'event-joueur' :
		switch($method) {
			case 'GET' : 
				echo getAnEventJoueur($complement);
				break;
			default:
				http_response_code('404');
				echo 'Diantre !';
		}
		break;
		
	case 'event-global-count' : 
		switch($method) {
			case 'GET' : 
				echo countEventGlobaux();
				break;
			default:
				http_response_code('404');
				echo 'Diantre !';
		}
		break;

	case 'event-joueur-count' :
		switch($method) {
			case 'GET' : 
				echo countEventJoueurs();
				break;
			default:
				http_response_code('404');
				echo 'Diantre !';
		}
		break;

	default : 
		http_response_code('500');
		echo 'unknown endpoint';
		break;
}
			