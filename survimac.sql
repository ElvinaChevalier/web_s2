-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : Dim 30 mai 2021 à 16:03
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `survimac`
--

-- --------------------------------------------------------

--
-- Structure de la table `corres_joueurs_equipes`
--

DROP TABLE IF EXISTS `corres_joueurs_equipes`;
CREATE TABLE IF NOT EXISTS `corres_joueurs_equipes` (
  `id_joueur` int(11) NOT NULL,
  `id_equipe` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

DROP TABLE IF EXISTS `equipe`;
CREATE TABLE IF NOT EXISTS `equipe` (
  `id` int(11) NOT NULL,
  `nom` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `couleur` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nb_joueurs` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Déchargement des données de la table `equipe`
--

INSERT INTO `equipe` (`id`, `nom`, `couleur`, `nb_joueurs`) VALUES
(1, 'Team 1', 'rouge', 0),
(2, 'Team 2', 'bleu', 0),
(3, 'Team 3', 'vert', 0),
(4, 'Team 4', 'jaune', 0),
(5, 'Team 5', 'orange', 0),
(6, 'Team 6', 'violet', 0),
(0, 'Tout le monde', 'blanc', 0),
(7, 'Année 1', 'blanc', 0),
(8, 'Année 2', 'blanc', 0),
(9, 'Année 3', 'blanc', 0),
(10, 'Papimac', 'blanc', 0),
(11, 'Prof', 'blanc', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ev_globaux`
--

DROP TABLE IF EXISTS `ev_globaux`;
CREATE TABLE IF NOT EXISTS `ev_globaux` (
  `id` int(11) NOT NULL,
  `nom` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `generation_touchee` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `effet_H` int(11) NOT NULL,
  `effet_M` int(11) NOT NULL,
  `effet_F` int(11) NOT NULL,
  `effet_P` int(11) NOT NULL,
  `used` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Déchargement des données de la table `ev_globaux`
--

INSERT INTO `ev_globaux` (`id`, `nom`, `description`, `generation_touchee`, `effet_H`, `effet_M`, `effet_F`, `effet_P`, `used`) VALUES
(1, 'Il pleut des cordes ', 'il pleut littéralement des cordes *aie ça fouette* :\'(', '0', -2, 0, 0, -1, 0),
(2, 'Organisation du Gala', 'Trop chouette, on fait bientôt la fête ! beaucoup d\'organisation à l\'horizon...', '8', 0, 0, 0, -2, 0),
(3, 'Le WEI est annulé', 'sad react only', '0', 0, 0, -5, 0, 0),
(4, 'Le Gala est annulé', 'sad react only (en plus c\'était sur une péniche *~* )', '0', 0, 0, -3, 0, 0),
(5, 'Gala de folie !', 'M. Cherrier organise le gala chez lui ! ', '0', 0, 0, 2, 0, 0),
(6, 'Cherrier VS Covid', 'Les cheveux de cherrier ont tué le covid ! C\'est incroyable !!! et pour fêter ça, pizza !', '0', 0, 0, 1000, 0, 0),
(7, 'Plus de café...', 'Le distributeur est en panne. Impossible de rester éveillé en classe.', '0', 0, 0, 0, -1, 0),
(8, 'Les cookies de Jules', 'Quoi de mieux après un partiel que les savoureux cookies de Jules (notre sauveur). Yummy ! ', '7', 2, 0, 0, 0, 0),
(9, 'TD de maths', 'Surfruiiiiiiiiiiiiiise un TD de maths a été ajouté à l\'EDT ! RIP ta grasse maths (t\'as compris ?) ', '7', 0, 0, -2, 1, 0),
(10, 'CC de signal', 'Sacrebleu ! Le CC de signal tombe juste après les vacances. Pas le choix, pour réussir il va falloir les sacrifier pour travailler. ', '7', 0, 0, -2, 1, 0),
(11, 'Riz Volution ! ', 'On fait tout sauter ! à la pooooêle.', '11', -3, 0, 0, 0, 0),
(12, 'Force soleil', 'Le soleil brille, il est temps d\'en profiter lors d\'un petit pic nique devant Coper ! ', '0', 0, 1, 1, 0, 0),
(13, 'OnO le Soleil', 'Le soleil se cache ! Mais allons-nous annuler le pic niquer devant Coper pour autant ? ah ah ah ce serait mal nous connaître... mais cela n\'est pas sans conséquences ! ', '0', -1, 0, 1, 0, 0),
(14, 'Bzzzt', 'Des aliens enlèvent un prof. Quelle jolie soucoupe ! ', '11', 0, -2, 0, 0, 0),
(15, 'CIP', 'Tu pensais y échapper ce semestre ? Hé bien non, il revient te hanter sans relâche ! aller hop hop hop\r\nPS : pense bien à faire ton meilleur power point ;) ', '0', 0, -2, -1, 1, 0),
(16, 'Des vacances ? ', 'Les vacances sont remplacées par un Workshop écriture ! C\'est moins reposant mais on y travaille sur un projet perso', '7', -1, 0, -1, 1, 0),
(17, 'Wallace & Gromit', 'La terre est en réalité un fromage ! Trop miam', '0', 2, 0, 1, -3, 0),
(18, 'Raclette Tartiflette', 'Tant que ya du fromage, je dis oui !', '0', 1, 0, 3, -1, 0),
(19, 'Des partiels ?', 'Des partiels pop dans l\'EDT ! Allez en prison sans passer par la case départ MWAHAHA', '0', 0, 0, -2, 0, 0),
(20, 'Jeudi ?', 'J\'arrive pas à croire qu\'on est déjà vendredi ! Elvina a oublié d\'envoyer le GIF poisson du jeudi, tout le monde est déçu', '0', 0, 0, -1, 0, 0),
(21, 'Chèèèvre', 'Des chèvres débarquent sur le campus ! Quel plaisir de les voir sautiller partout', '0', 0, 2, 5, 0, 0),
(22, 'Un monde parfait', 'Un oiseau, un enfant, une chèvre\r\nle bleu du ciel, un beau sourire du bout des lèvres\r\nun crocodile, une vache, du soleil\r\net ce soir je m\'endors au pays des merveilles ! \r\nImpossible de se sortir cet air de la tête', '0', 0, -3, 3, 0, 0),
(23, 'Crêpes party', 'Impossible de résister à une proposition si alléchante. ', '0', 0, 1, 1, -1, 0),
(24, 'Chocapics ', 'C\'est Pâques, Epifree distribue des chocolats dans les couloirs, tout le monde est content ! Ça fait des chocapics ! ', '0', 0, 2, 1, 0, 0),
(25, 'Evènement ?', 'C\'est très calme aujourd\'hui, on dirait qu\'il ne va rien se passer... (ok c\'est vrai, on avait besoin d\'un évènement supplémentaire) ', '0', 0, 1, 0, 0, 0),
(26, 'Le temps des cerises', 'Tout le monde part à la cueillette ! ', '0', 0, 1, 1, 0, 0),
(27, 'CROUS en marche !', 'Le CROUS décide de mettre en place des repas végé trois fois par semaine ! Quelle bonne initiative, tout le monde a soudainement l\'air plus gentil ! ', '0', 3, 0, 0, 0, 0),
(28, 'MEGA\'Périmé', 'Les profs ont été changés cette année, le MEGAPACK est complètement obsolète, il faut tout recommencer ! ', '7', 0, 0, 0, -1, 0),
(29, 'Incendie', 'Anaïs a déclenché l\'alarme incendie, le bâtiment est évacué, le cours se transforme en pic-nique improvisé !', '8', 0, 0, 1, -1, 0),
(30, 'Dérat\'IMAC', 'Le bâtiment est fermé à cause de la libération d\'un gros rat mutant bleu par le club de chimie. La vache, il a trouvé comment se reproduire tout seul, c\'est monstrueux ! Les profs ne peuvent plus faire cours...', '11', 0, 0, 0, -2, 0),
(31, 'JPO\'hoho', 'Le père Noël fait une apparition à la JPO cette année, c\'est merveilleux, tout le monde veut rejoindre l\'école ! ', '0', 0, 2, 3, 0, 0),
(32, 'Uku\'Lundi', 'Elvina donne de l\'amour à tout le monde avec son petit morceau du lundi au ukulele, ça met du soleil dans nos coeurs !', '0', 0, 2, 0, 0, 0),
(33, 'Master Souffrance', 'Les 3e années sont sous l\'eau et ne savent pas nager. Les requins-cours dévorent leur temps et leur énergie. Leur état de santé alarme et les Babimacs décident de ne jamais faire de master !', '9', -2, -2, -1, 0, 0),
(34, 'Cheveux Blancs', 'Les 3e années se rendent compte que dans peu de temps, ils seront PAPIMACS. Ça fait quand même un coup au moral...', '9', 0, -1, 0, 0, 0),
(35, 'Parrain désoeuvré', 'On est en mai, et les babimacs n\'ont toujours pas fait leurs gages. Les parrains et marraines sont inconsolables. ', '8', 0, 0, -2, 0, 0),
(36, '25H de la mort', 'C\'est enfin les 25H de la BD, tout le monde est motivé et remet à plus tard toutes ses corvées (y compris dormir) ! ', '0', -1, 1, 2, -1, 0),
(37, 'La fête du slip', '1 avril - C\'est la fameuse fête du slip : tout le monde doit venir en sous-vêtement, malheureusement, comme chacun sait \"en avril ne te découvre pas d\'un fil\". Evidemment, tout le monde attrape un vilain rhume, mais c\'était très drôle. ', '0', -1, 1, 2, 0, 0),
(38, 'Bonne nuit les...', 'Le marchand de sable passe au dessus du campus, tout le monde sombre dans un profond sommeil et au réveil, personne ne se sent pareil ! C\'est magique (PS : bonus si quelqu\'un s\'appelle Pimprenelle) ', '0', 3, 1, 0, 0, 0),
(39, 'Foudre foudroyante', 'La foudre s\'abat sur le bâtiment Copernic, le courant est coupé pendant un jour entier. L\'école se transforme, c\'est l\'anarchie ! ', '0', 0, 0, 2, -2, 0),
(40, 'Grave grève', 'Les profs sont en grève (et moi aussi)', '11', 0, 0, 0, -1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ev_joueurs`
--

DROP TABLE IF EXISTS `ev_joueurs`;
CREATE TABLE IF NOT EXISTS `ev_joueurs` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `effet_H` int(11) NOT NULL,
  `effet_M` int(11) NOT NULL,
  `effet_F` int(11) NOT NULL,
  `effet_P` int(11) NOT NULL,
  `used` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Déchargement des données de la table `ev_joueurs`
--

INSERT INTO `ev_joueurs` (`id`, `nom`, `description`, `effet_H`, `effet_M`, `effet_F`, `effet_P`, `used`) VALUES
(1, 'Projets', 'Le joueur est en retard sur ses projets, il n\'a plus le temps de s\'amuser !', 0, -4, 0, 0, 0),
(2, 'Chamallow', 'Cherrier transforme le joueur en chamallow !', 2000, 2000, 2000, -2000, 0),
(3, 'Pas d\'prof pour le contrôle', 'Yvan est venu pour un contrôle ! Le prof, surpris, s\'est évanoui et ne pourra pas surveiller le contrôle, \"Youp\'Yvan !\" devient le nouveau cri de guerre de la classe !  ', 0, 0, 0, -2, 0),
(4, 'Pic-nic foireux', 'Le pic-nic tourne mal... le joueur est bourré en cours, quelle mauvaise idée... ', 0, 0, 1, -3, 0),
(5, 'Chute ', 'Le joueur glisse sur du gel hydroalcoolique à l’entrée parce que quelqu\'un a appuyé trop fort 2min avant, ça fait mal aux fesses !', -2, 0, -1, 0, 0),
(6, 'Bus raté', 'Le joueur s\'est levé du pied gauche et a loupé son bus/RER, pas cool ', 0, 0, -1, -1, 0),
(7, 'Poison', 'Le joueur a voulu prendre un repas au CROUS, manque de pot, il est empoisonné !', -4, 0, 0, 0, 0),
(8, 'Des pates, des pates et encore des pates...', 'Le frigo est vide, le joueur mange encore des pâtes au pesto sur un coin de bureau en rêvant des ptits plats de sa maman', -1, -1, 0, 0, 0),
(9, 'La déter', 'Le joueur se tape une motiv et bosse comme un ouf jusqu’à 3h du mat', 0, -2, 0, 2, 0),
(10, 'Panne d\'oreiller', 'Le joueur oublie de se réveiller (il savait bien qu\'il aurait du le noter sur un post-it)', 0, 1, 0, -1, 0),
(11, 'La gaufre anonyme', 'La gaufre anonyme a volé le travail du joueur.', 0, 0, 0, -1, 0),
(12, 'Le FBI', 'Le FBI contrôle le joueur dans la rue. S\'il n\'a pas le ticket d\'achat de son ordinateur, il gagne 3 fromages...', 0, -1, 0, 0, 0),
(13, 'Fait froid', 'Le joueur tourne son court-métrage d’audiovisuel en extérieur en décembre, RIP ses doigts.\r\n', -2, 0, 0, 0, 0),
(14, 'Enregistrement non registré', 'Le rendu de partiel du joueur ne s\'est pas enregistré dans le bon fichier, il va devoir aller au rattrapage... ', 0, -1, 0, -1, 0),
(15, 'IMACannes ', 'Le court-métrage du joueur a reçu un oscar à la cérémonie des IMACannes ! Félicitations ! \r\n', 0, 1, 1, 0, 0),
(16, 'Traquenard', 'Le joueur se fait traquenarder en after par un Papimac, il rentre super tard mais avec des anecdotes folles à raconter ! ', 0, 0, 1, -1, 0),
(17, 'Plus d\'thunes', 'Le joueur a dépensé tout son argent du mois en vodka, RIP le petit dej...', -2, -1, 1, 0, 0),
(18, 'Trousse pour un', 'Le joueur participe à l\'atelier trousse pour un du jeudi soir, trop bien !', 0, 1, 2, 0, 0),
(19, 'Le four', 'Le joueur prépare un gâteau d’anniversaire pour quelqu\'un, mais il est cramé par son four ! ça pique ! ', 0, 0, -1, 0, 0),
(20, 'GDRSA (Coup Dur)', 'Un prof anonyme change encore les consignes du projet 2 jours avant le rendu, le joueur passe sa nuit à tout changer : RIP sa santé mentale.\r\n', -3, -3, -1, 0, 0),
(21, 'Chèvres ', 'Le joueur quitte l’imac pour aller élever des chèvres dans le Jura  : il quitte le jeu.', 9999, 9999, 9999, -999, 0),
(22, 'Stage, où es-tu ?', 'Le joueur n\'a toujours pas trouvé de stage, c\'est la panique ! ', 0, -2, 0, 0, 0),
(23, 'La coloc, c\'est chouette !', 'Les colocs du joueur ont fini leurs partiels et organise une soirée alors que le joueur est en plein dans le rush de fin de semestre. Penser à acheter des bouchons d\'oreille ! ', 0, -1, -2, -1, 0),
(24, 'Sasha VS les toilettes ', 'Sasha part au toilette pendant la session de travail sur discord, il se mute et tout le monde arrive soudainement mieux à se concentrer ! ', 0, 0, 0, 1, 0),
(25, 'Sasha VS les toilettes (le retour)', 'Sasha part au toilette pendant la réunion discord. OnoO il a oublié de se mute ! ', 0, 0, 1, -1, 0),
(26, 'Rêve de Jura', 'Le joueur décide sur un coup de tête de partir élever des chèvres dans le Jura, il quitte l\'école et fait verdir de jalousie tous ses amis ! ', 10, 10, 10, -100, 0),
(27, 'La gourmandise associative', 'Le joueur se rend compte qu\'il a été trop enthousiaste et s\'est engagé dans trop d\'associations, il n\'a plus le temps de dormir, Oskour ! ', -4, -1, 0, -2, 0),
(28, 'Matrix', 'Le joueur se rend compte qu\'il est dans une simulation virtuelle type Hunger Games de l\'IMAC, mais contrairement à Némo, pas de Morpheus à l\'horizon pour le sortir de là... Le joueur devient tout simplement fou !  ', -4, -10, 0, 0, 0),
(29, 'Annivèèèèrsaire', 'C\'est encore l\'anniversaire d\'un copain, le joueur doit mettre en place une expédition pour signer la carte d\'anniversaire et aller chercher un cadeau à la fnac ! Que d\'aventures ! ', 0, 0, 1, -1, 0),
(30, 'Deadline', 'Le joueur a enfin fini son projet ! Pfiouuu, il lui reste 30 minutes avant la deadline pour rédiger un rapport de 10 pages >.<', 0, 0, -1, -1, 0),
(31, 'Reprise en main !', 'C\'est le printemps, le joueur décide de se remettre au sport et d\'arrêter les chips et les sucreries ! Il est beau comme un coeur, quelle réussite ! ', 3, 1, 0, 0, 0),
(32, 'Grosse gamelle ', '14H00 - Le joueur est si fatigué qu\'il tombe dans ses escaliers, ne parvient pas à se relever et s\'endort sur place. \r\n17H30 - Le joueur se réveille enfin (au milieu d\'une flaque de sang) et ne se rappelle de rien... De pire en pire !  ', -2, -1, -1, 0, 0),
(33, 'Visite de Cupidon <3', 'Le joueur est touché par une flèche et tombe fou amoureux. Il ne peut plus penser à rien d\'autre qu\'à l\'objet de son amour, il écrit des poèmes à longueur de journée et oublie de travailler...', 0, 2, 0, -2, 0),
(34, 'WE de famille', 'Le joueur rentre chez ses parents pour le week-end, il avait oublié le goût des légumes et se sent tout à coup plus vivant (et un peu plus lourd aussi) ! ', 2, 1, 0, 0, 0),
(35, 'Lavomatique de l\'enfer', 'Le joueur n\'a plus de chaussettes propres et décide d\'aller à la laverie. Malheureusement, seule une machine fonctionne et la queue est longue comme son bras, il arrive donc en retard en amphi de maths...', 0, -1, -1, -2, 0),
(36, 'Caféine de l\'extrême', 'Le joueur a bu son poids en café pour réussir à travailler, l\'effet escompté est atteint, malheureusement, il ne trouve plus le sommeil et fait de la tachycardie ', -2, 0, 0, 1, 0),
(37, 'Ennui mortel', 'Le joueur, en visite pour l\'anniversaire de sa mamie, est coincé dans le patelins de ses grands-parents. Rien à faire ! Entre le scrabble et la vaisselle, il n\'a rien d\'autre à faire que travailler.', 0, 0, -1, 3, 0),
(38, 'Atchoum', 'Le printemps arrive et réveille les allergies du joueur, ses yeux et son nez triplent de volumes, on le prend pour un monstre dans la rue. Le joueur décide de se cloitrer chez lui pour une durée indéterminée. ', -2, 0, 0, -1, 0),
(39, 'Monsieur Propre', 'Horreur, le joueur réalise que du moisi pousse dans ses toilettes, il a encore oublié de faire le ménage. Cette fois pas d\'excuse, il s\'y met sur le champs ! ', 1, 1, -1, 0, 0),
(40, 'Dring Driiing !', 'On sonne à la porte du joueur, il se précipite naïvement pour récupérer sa pizza. Enfer et damnation ! Ce sont les témoins de Jéhovah qui lui tiennent la jambe pendant trois loooongues heures', 0, -1, -1, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `joueur`
--

DROP TABLE IF EXISTS `joueur`;
CREATE TABLE IF NOT EXISTS `joueur` (
  `id` int(11) NOT NULL,
  `nom` text COLLATE utf8_general_mysql500_ci NOT NULL,
  `prenom` text COLLATE utf8_general_mysql500_ci NOT NULL,
  `H` int(11) NOT NULL DEFAULT '10',
  `M` int(11) NOT NULL DEFAULT '10',
  `F` int(11) NOT NULL DEFAULT '10',
  `P` int(11) NOT NULL DEFAULT '10',
  `generation` int(11) NOT NULL,
  `etat` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Déchargement des données de la table `joueur`
--

INSERT INTO `joueur` (`id`, `nom`, `prenom`, `H`, `M`, `F`, `P`, `generation`, `etat`) VALUES
(1, 'jojo', 'jojo', 10, 10, 10, 10, 7, 1),
(2, 'Kolok', 'Kaaris', 10, 10, 10, 10, 10, 1),
(3, 'Bouchy', 'Juju', 10, 10, 10, 10, 7, 1),
(4, 'Kolok', 'Hélène', 10, 10, 10, 10, 8, 1),
(5, 'Bar', 'Lény', 10, 10, 10, 10, 10, 1),
(6, 'Régnéfonpeur', 'César', 10, 10, 10, 9, 10, 1),
(7, 'Stiké', 'Sophie', 10, 10, 10, 10, 9, 1),
(8, 'La Frite', 'Jean', 10, 10, 10, 10, 8, 1),
(9, 'Lermitte', 'Bernard', 10, 10, 10, 10, 9, 1),
(10, 'Trouduc', 'Queutard', 10, 10, 10, 10, 8, 1),
(11, 'Mora', 'Nadine', 10, 10, 9, 9, 7, 1),
(12, 'Urdor(t)eille', 'Ted', 10, 10, 10, 10, 8, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
